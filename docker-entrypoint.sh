#!/bin/bash
set -e

files=( "z-push.conf.php" "policies.ini" "imap.conf.php" "combined.conf.php" "carddav.conf.php" "caldav.conf.php" "autodiscover.conf.php" )

if [ "$1" = 'apache2ctl' ]; then

	for i in "${files[@]}"
	do
		if [ ! -e /etc/z-push/$i ]; then
			cp /config/$i /etc/z-push/$i
		fi
	done

    exec apache2ctl -D FOREGROUND
fi

exec "$@"