FROM ubuntu:xenial

RUN apt update && apt install -y wget

RUN echo "deb http://repo.z-hub.io/z-push:/final/Ubuntu_16.04/ /" > /etc/apt/sources.list.d/z-push.list && \
	wget -qO - http://repo.z-hub.io/z-push:/final/Ubuntu_16.04/Release.key | apt-key add - && \
	apt update && \
	apt install -y z-push-backend-caldav z-push-backend-carddav z-push-backend-combined z-push-backend-imap z-push-common z-push-ipc-sharedmemory z-push-config-apache z-push-autodiscover z-push-config-apache-autodiscover php-mail-mimedecode && \
	echo 'include_path = ".:/usr/share/php:/usr/share/awl/inc"' >> /etc/php/7.0/apache2/php.ini

RUN mkdir /config && \
	cp /etc/z-push/* /config

ADD docker-entrypoint.sh /

RUN sed -i "s/\"\%u\",\ \$username,/\"\%u\",\ Utils\:\:GetLocalPartFromEmail\(\$username\),/g" /usr/share/z-push/backend/carddav/carddav.php && \
	sed -i "s/\$this->username\ =\ \$username;/\$this->username\ =\ Utils\:\:GetLocalPartFromEmail\(\$username\);/g" /usr/share/z-push/backend/carddav/carddav.php && \
	sed -i "s/'\%u',\ \$username,/'\%u',\ Utils\:\:GetLocalPartFromEmail\(\$username\),/g" /usr/share/z-push/backend/caldav/caldav.php

EXPOSE 80
VOLUME ["/etc/z-push", "/var/lib/z-push/"]

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["apache2ctl"] 